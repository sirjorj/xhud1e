#pragma once
#include "./libxwing/squad.h"
#include "./libxwing/dice.h"
#include "settings.h"
#include <string>

using namespace libxwing;

struct Dice {
  enum class State {
    None,
    Attacking,
    Defending
  } state;
  AttackDice  attackDice;
  DefenseDice defenseDice;
};


/*
struct Card {
  enum class Type {
    Pilot,
    Upgrade,
    Damage
  } type;
};
*/


struct ImageGenToken {
  SettingsManager &settings;
  Squad& p1squad;
  Squad& p2squad;

  // add more stuff here...

  uint8_t initiative;

  Dice dice[2];

  //Card card;
};



void GenerateImage(Squad& list, std::string name);
void GenerateImage(ImageGenToken token);

std::string GetGdVersion();
