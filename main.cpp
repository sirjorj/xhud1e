#include "colors.h"
#include "printhelpers.h"
#include "settings.h"
#include "imagegen.h"
#include "game.h"
#include "builder.h"
#include "portable.h"
#include "libxwing/release.h"
#include "libxwing/test.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <algorithm>
#include <experimental/optional>
#include <fstream>
#include <iostream>
#include <string.h>



const static std::string VERSTR = "0.9";

using namespace libxwing;

std::string GetVersionInfo() {
  return "xhud1e v" + VERSTR + " (using libgd " + GetGdVersion() + ")";
}

static void printOptions() {
  printf("%s\n", GetVersionInfo().c_str());
  printf("Usage: './xhud1e *option* {params}'\n");

    printf("Options:\n");
    printf("  check             - check for required files\n");
    printf("  settings          - read and display the settings file\n");
    printf("  ship              - prints available ships\n");
    printf("  ship {S}          - prints details on ship {s}\n");
    printf("  pilot             - prints available pilots\n");
    printf("  pilot {P F S}     - prints details on pilot {P F S} (Pilot, Faction, Ship)\n");
    printf("  upgrade           - prints available upgrades\n");
    printf("  upgrade {T U}     - prints details on upgrade {T U} (Type, Upgrade)\n");
    printf("  release           - prints all releases\n");
    printf("  release {SKU}     - prints details on release {SKU}\n");
    printf("                      NOTE: {S}, {P}, {T}, and {U} are the xws keys\n");
    printf("  squad {SQ}        - dump the squad {SQ} to terminal ({SQ} is a .xws file)\n");
    printf("  verify {SQ}       - verify the squad (SQ)\n");
    printf("  img {L} {I}       - generate image (I) for the list (L)\n");
    printf("  run {L1} {L2} {P} - run a game with the 2 specified lists, outputting image(s) in directory at path (P)\n");
    printf("  build {f}         - start the squad builder\n");
}



static bool fileExists(const std::string file) {
  struct stat buffer;
  return (stat (file.c_str(), &buffer) == 0);
}



static std::vector<std::pair<std::string,bool>> CheckFonts() {
  std::vector<std::string> fontFiles = {
    { "xwing-miniatures-ships.ttf"  },
    { "xwing-miniatures.ttf"        },
    { "SquarishSansCTRegularSC.ttf" },
    { "xwstats.ttf"                 }
  };
  std::vector<std::pair<std::string,bool>> ret;
  for(auto ff : fontFiles) {
    std::string filename = "./fonts/" + ff;
    bool hasIt = false;
    if(fileExists(filename)) {
      hasIt = true;
    }
    ret.push_back({ff, hasIt});
  }
  return ret;
}



bool VerifyList(std::string listFile) {
  //printf("Verifying %-36s - ", listFile.c_str());
  fflush(stdout);
  try {    
    Squad sq = Squad(listFile);
    std::vector<std::string> issues = sq.Verify();
    if(issues.size() == 0) {
      printf("Ok\n");
    } else {
      printf("INVALID\n");
      for(std::string s : issues) {
	printf("  \e[1;31m%s\x1B[0m\n", s.c_str());
      }
    }
    if(issues.size() > 0) {
      return false;
    } else {
      return true;
    }
  }
  catch(std::invalid_argument e) {
    printf("EXCEPTION\n");
    printf("  \e[1;31m%s\x1B[0m\n", e.what());
    return false;
  }
}

std::string GetConfigFile() {
  std::string configPath;
  char *home = std::getenv("HOME");
  if(home) {
    configPath += home;
  }
  configPath += "/.xhud1e.conf";
  return configPath;
}

Squad MakeSquad(std::string file) {
  if(file == "--") {
    return Squad(std::cin);
  } else {
    return Squad(file.c_str());
  }
}

#include <unistd.h>

// **************
// *** main() ***
// **************
int main(int argc, char *argv[]) {

  if(pledge("stdio rpath wpath cpath", NULL) == -1) err(1, "pledge");

  SettingsManager settings = SettingsManager(GetConfigFile());

  if(argc == 1) {
    printOptions();
  }

  else if(strcmp(argv[1], "settings") == 0) {
    settings.VerifySettings(0);
    settings.Dump();
  }

  else if(strcmp(argv[1], "check") == 0) {
    printf("Checking for required fonts...\n");
    std::vector<std::pair<std::string,bool>> cf = CheckFonts();
    int fontlen=0;
    for(auto f : cf) { if(f.first.length() > fontlen) fontlen = f.first.length(); };
    for(auto f : cf) { printf("  %-*s - %s\n", fontlen, f.first.c_str(), f.second ? "Ok" : "NOT FOUND"); }
  }

  else if(strcmp(argv[1], "sanity") == 0) {
    Pilot::SanityCheck();
    printf("\n");
    Upgrade::SanityCheck();
    return 0;
  }

  else if((strcmp(argv[1], "squadtest") == 0) && (argc == 3)) {
    RunTestXwsFiles(argv[2]);
  }

  else if((strcmp(argv[1], "ship") == 0)) {
    if     (argc == 2) { PrintShips();        }
    else if(argc == 3) {
      try {
        PrintShip(Ship::GetShip(argv[2]));
      } catch(ShipNotFound snf) {
        std::vector<Ship> ss = Ship::FindShip(argv[2]);
        if(ss.size() == 1) { PrintShip(ss[0]); }
        else               { PrintShips(ss); }
      }
    }
    else               { printOptions(); }
  }

  else if((strcmp(argv[1], "pilot") == 0)) {
    if     (argc == 2) { PrintPilots(); }
    else if(argc == 3) { std::vector<Pilot> ps = Pilot::FindPilot(argv[2]);
                         if(ps.size() == 1) { PrintPilot(ps[0]); }
                         else               { PrintPilots(ps); }
    }
    else if(argc == 5) { PrintPilot(Pilot::GetPilot(argv[2], argv[3], argv[4])); }
    else               { printOptions(); }
  }

  else if((strcmp(argv[1], "upgrade") == 0)) {
    if     (argc == 2) { PrintUpgrades(); }
    else if(argc == 3) { std::vector<Upgrade> us = Upgrade::FindUpgrade(argv[2]);
                         if(us.size() == 1) { PrintUpgrade(us[0]); }
                         else               { PrintUpgrades(us); }
    }
    else if(argc == 4) { PrintUpgrade(Upgrade::GetUpgrade(argv[2], argv[3])); }
    else               { printOptions(); }
  }

  else if((strcmp(argv[1], "release") == 0)) {
    if     (argc == 2) { PrintRelease(); }
    else if(argc == 3) { PrintRelease(argv[2]); }
    else               { printOptions(); }
  }

  else if((strcmp(argv[1], "squad") == 0) && (argc==3)) {
    Squad sq = MakeSquad(argv[2]);
    PrintSquad(sq);
  }

  else if((strcmp(argv[1], "verify") == 0) && (argc==3)) {
    VerifyList(argv[2]);
  }

  else if((strcmp(argv[1], "img") == 0) && (argc==4)) {
    Squad sq = MakeSquad(argv[2]);
    GenerateImage(sq, argv[3]);
  }

  else if((strcmp(argv[1], "run") == 0) && (argc>=4)) {
    bool cannotPlay = false;
    std::string f1 = argv[2];
    std::string f2 = argv[3];

    // run
    printf("Running %s\n", GetVersionInfo().c_str());
    printf("\n");

    // verify we have the fonts
    printf("Checking fonts...\n");
    {
      std::vector<std::pair<std::string,bool>> cf = CheckFonts();
      int fontlen=0;
      for(auto f : cf) { if(f.first.length() > fontlen) fontlen = f.first.length(); };
      for(auto f : cf) {
        printf("  %-*s - ", fontlen, f.first.c_str());
        if(f.second) {
          printf("Ok\n");
        } else {
          printf("%sNOT FOUND%s\n", CLR_ERR, CLR_DEF);
          cannotPlay = true;
        }
      }
    }
    printf("\n");

    // verify the lists are present and valid
    printf("Checking lists...\n");
    {
      int listlen = f1.length();
      if(f2.length() > listlen) { listlen = f2.length(); }
      for(auto f : {f1, f2}) {
        printf("  %-*s - ", listlen, f.c_str());
        if(!fileExists(f)) {
          printf("%sERROR\n    File not found%s\n", CLR_ERR, CLR_DEF);
          cannotPlay = true;
        } else {
          std::vector<std::string> issues = Squad(f).Verify();
          if(issues.size() > 0) {
            printf("%sISSUES%s:\n", CLR_WRN, CLR_DEF);
            for(std::string i : issues) {
              printf("    %s%s%s\n", CLR_WRN, i.c_str(), CLR_DEF);
            }
          } else {
            printf("Ok\n");
          }
        }
      }
    }
    printf("\n");

    // figure out what outdir to use
    printf("Checking outdir...\n");
    {
      // if outout file was passed in as argument, use it
      printf("Checking output file...\n");
      if(argc>=5) {
	printf("  using outdir from command line\n");
	settings.SetSetting("run.outdir", argv[4]);
      }

      // if the output file starts with '~', resolve it to home dir
      // user *can* resave this but doesn't have to
      //std::string outdir = settings.GetSetting("run.outdir");
      //if(outdir[0] == '~') {
      //  printf("  resolving ~\n");
      //  outdir.replace(0, 1, std::getenv("HOME"));
      //  settings.SetSetting("run.outdir", outdir);
      //}

      if(settings.GetSetting("run.outdir") == "") {
	printf("%sERROR\n    No outdir specified%s\n", CLR_ERR, CLR_DEF);
	cannotPlay = 1;
      }
    }
    printf("\n");

    if(cannotPlay) return 0;

    // run the game
    printf("Running game...\n");
    try{
      std::array<Squad, 2> players = { { Squad(f1), Squad(f2) } };
      Game g = Game(players, settings);
      g.Run();
    }
    catch(std::invalid_argument ia) {
      printf("Error loading list '%s'\n", ia.what());
      return 1;
    }
  }

  else if((strcmp(argv[1], "build") == 0) && (argc == 3)) {
    std::experimental::optional<SquadBuilder> sb;
    std::string arg = argv[2];
    if((arg == "imperial") || (arg == "rebel") || (arg == "scum")) {
      sb = SquadBuilder(Squad(Faction::GetFaction(argv[2]).GetType()));
    }
    else if(fileExists(arg)) {
      sb = SquadBuilder(std::string(argv[2]));
    }
    else {
      printf("Please specify faction for new squad ('imperial', 'rebel', or 'scum')\n");
      printf("Or fliename of existing squad\n");
      return 1;
    }
    sb->Run();
  }

  else {
    printOptions();
  }

  return 0;
}
