#pragma once
#include "libxwing/release.h"
#include "libxwing/squad.h"
#include "libxwing/release.h"
#include "libxwing/upgrade.h"
#include <algorithm>

using namespace libxwing;

std::string GetDifficultyColor(Difficulty d);
void        PrintManeuverChart(Maneuvers maneuvers);
void        PrintShips(std::vector<Ship> ships=Ship::GetAllShips());
void        PrintShip(Ship ship);
void        PrintPilots(std::vector<Pilot> pilots=Pilot::GetAllPilots());
void        PrintPilot(Pilot p);
void        PrintUpgrades(std::vector<Upgrade> upgrades=Upgrade::GetAllUpgrades());
void        PrintUpgrade(Upgrade u);
void        PrintRelease();
void        PrintRelease(std::string sku);
void        PrintSquad(Squad s);
