#!/usr/bin/perl

mkdir "./test/";

@pilots = `../xhud pilot`;
print "Pilots";
foreach my $pilot (@pilots) {
    $pilot =~ s/^\s+//; # remove leading spaces
    $pilot =~ s/\s+$//; # remove trailing spaces
    $pilot =~ s/\x1b\[[0-9;]*m//g; # remove the terminal color codes

    @tokens = split / +/, $pilot;

    $plt = $tokens[0];
    $fac = $tokens[1];
    $shp = $tokens[2];

    my $xwsTemplate;
    my $filename = "./pilottest.xws.tmpl";

    open(my $fh, '<', $filename) or die "cannot open file $filename - $!";
    {
	local $/;
	$xwsTemplate = <$fh>;
    }
    close($fh);

    $xwsTemplate =~ s/{FACTION}/$fac/;
    $xwsTemplate =~ s/{PILOT}/$plt/;
    $xwsTemplate =~ s/{SHIP}/$shp/;

    $command = "cd .. && echo '$xwsTemplate' | ./xhud img -- ./testing/test/plt_$shp\_$fac\_$plt.png";

    system($command);
    print ".";
    #exit;
}
print "\n";

@upgrades = `../xhud upgrade`;
print "Upgrades";
foreach my $upgrade (@upgrades) {
    $upgrade =~ s/^\s+//; # remove leading spaces
    $upgrade =~ s/\s+$//; # remove trailing spaces
    $upgrade =~ s/\x1b\[[0-9;]*m//g; # remove the terminal color codes

    @tokens = split / +/, $upgrade;

    $typ = $tokens[0];
    $upg = $tokens[1];

    my $xwsTemplate;
    my $filename = "./upgradetest.xws.tmpl";
    
    open(my $fh, '<', $filename) or die "cannot open file $filename - $!";
    {
	local $/;
	$xwsTemplate = <$fh>;
    }
    close($fh);

    $xwsTemplate =~ s/{TYPE}/$typ/;
    $xwsTemplate =~ s/{UPGRADE}/$upg/g;
    
    #print "$counter) $typ - $upg\n";

    $command = "cd .. && echo '$xwsTemplate' | ./xhud img -- ./testing/test/upg_$typ\_$upg.png";

    system($command);
    print ".";
    #exit;
}
print "\n";
