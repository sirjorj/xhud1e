#include "printhelpers.h"
#include "colors.h"



std::string GetDifficultyColor(Difficulty d) {
  switch(d) {
  case Difficulty::Green: return CLR_MDG;
  case Difficulty::White: return CLR_MDW;
  case Difficulty::Red:   return CLR_MDR;
  }
}



void PrintManeuverChart(Maneuvers maneuvers) {
  int8_t min = 10;
  int8_t max = -10;
  for(auto a : maneuvers) { if(a.speed > max) {max = a.speed;} if(a.speed < min) {min=a.speed;} }

  Maneuver m;
  for(int i=max; i>=min; i--) {
    printf(" %s%1d|", CLR_TXT, i);

    // standard maneuvers
    for(Brn b : {Brn::LTurn, Brn::LBank, (i==0) ? Brn::Stationary : Brn::Straight, Brn::RBank, Brn::RTurn}) {
      if(FindManeuver(maneuvers, i, b, m)) {
        printf(" %s%s%s", GetDifficultyColor(m.difficulty).c_str(), Bearing::GetBearing(m.bearing).GetCharacter().c_str(), NORMAL);
      } else {
        printf("  ");
      }
    }

    // special maneuvers
    for(Brn b : {Brn::KTurn, Brn::LSloop, Brn::RSloop, Brn::LTroll, Brn::RTroll}) {
      if(FindManeuver(maneuvers, i, b, m)) {
        printf(" %s%s%s", GetDifficultyColor(m.difficulty).c_str(), Bearing::GetBearing(m.bearing).GetCharacter().c_str(), NORMAL);
      }
    }

    printf("\n");
  }
  // reverse maneuvers
  for(int i=min; i<=max; i++) {
    bool doIt = false;
    for(Brn b : {Brn::RevLBank, Brn::RevStraight, Brn::RevRBank}) {
      if(FindManeuver(maneuvers, i, b, m)) {
        doIt = true;
        break;
      }
    }
    if(doIt) {
      printf("%s-%1d|  ", CLR_TXT, i);
      for(Brn b : {Brn::RevLBank, Brn::RevStraight, Brn::RevRBank}) {
        if(FindManeuver(maneuvers, i, b, m)) {
          printf(" %s%s%s", GetDifficultyColor(m.difficulty).c_str(), Bearing::GetBearing(m.bearing).GetCharacter().c_str(), NORMAL);
        } else {
          printf("  ");
        }
      }
    }
  }
}



void PrintShips(std::vector<Ship> ships) {
  std::vector<std::string> done;
  for(auto s : ships) {
    bool isReleased = false;
    for(Release r : Release::GetAllReleases()) {
      if(!r.IsUnreleased()) {
        for(RelShip rs : r.GetShips()) {
          if(rs.ship == s.GetType()) {
            isReleased = true;
            break;
          }
        }
        if(isReleased) { break; }
      }
    }
    printf("%s%-30s %-30s %s%s\n", CLR_SHP,  s.GetName().c_str(), s.GetXws().c_str(), CLR_UNR, isReleased ? "" : "[UNRELEASED]");
  }
}

void PrintShip(Ship ship) {
  std::vector<Pilot> pilots;
  std::string name = "";
  Act         act  = Act::None;
  int nameLength = 0;
  Maneuvers maneuvers;
  Arc arc = Arc::None;
  for(Pilot p : Pilot::GetAllPilots()) {
    if(p.GetShip().GetType() == ship.GetType()) {
      if(arc==Arc::None)                    arc = p.GetShip().GetPrimaryArc().GetType();
      if(name=="")                          name = p.GetShip().GetName();
      if(act==Act::None)                    act = p.GetNatActions();
      if(nameLength < p.GetName().length()) nameLength = p.GetName().length();
      if(maneuvers.size() == 0)             maneuvers = p.GetNatManeuvers();
      pilots.push_back(p);
    }
  }

  std::sort(pilots.begin(), pilots.end(), [] (Pilot a, Pilot b) {
      return a.GetFaction().GetType() < b.GetFaction().GetType() || ((a.GetFaction().GetType() == b.GetFaction().GetType()) && (a.GetNatSkill() > b.GetNatSkill()));
    });

  printf("%s%s\n", CLR_SHP, name.c_str());
  //printf("%sArc: %s%s\n", CLR_LBL, CLR_TXT, PrimaryArc::GetPrimaryArc(arc).GetName().c_str());

  // print the maneuver chart
  printf("\n");
  PrintManeuverChart(maneuvers);
  printf("\n");

  // see if any of the ships have EPT
  bool shipHasEpt = false;
  for(auto p : pilots) {
    for(Upg u : p.GetNatPossibleUpgrades()) {
      if(u == Upg::Elite) { shipHasEpt = true; break; }
    }
    if(shipHasEpt) break;
  }

  // print the info
  for(auto p : pilots) {
    printf("%s%-6s %s%-2d %s%s%s%-*s%s[%-2d]  %s%-2d  %s%-2d  %s%-2d  %s%-2d%s",
           CLR_TXT, p.GetFaction().GetName().c_str(),
           CLR_SKL, p.GetNatSkill(), CLR_UNI, p.IsUnique() ? "*" : " ", CLR_PLT, nameLength+1, p.GetName().c_str(), CLR_CST, p.GetNatCost(),
           CLR_ATK, p.GetNatAttack(), CLR_AGI, p.GetNatAgility(), CLR_HUL, p.GetNatHull(), CLR_SHD, p.GetNatShield(), CLR_TXT);

    printf(" - ");
    ForEachAction(p.GetNatActions(), [](Act a) {printf("[%s]", Action::GetAction(a).GetShortName().c_str());});
    printf(" - ");

    int c = 0;
    for(Upg u : p.GetNatPossibleUpgrades()) {
      // if theres an EPT but not for this pilot, leave a space for it
      if(shipHasEpt && (c==2) && u != Upg::Elite) { printf("     "); }
      printf("[%s%s%s]", CLR_UPT, UpgradeType::GetUpgradeType(u).GetShortName().c_str(), CLR_TXT);
      c++;
    }
    if(shipHasEpt && (c==2)) {
      printf("     "); // stupid edge case...
    }

    printf(" - %s%s", p.HasAbility() ? CLR_TXT : CLR_LBL, p.GetText().c_str());

    printf(NORMAL"\n");
  }
}



void PrintPilots(std::vector<Pilot> pilots) {
  // get string lengths
  int facLen=0;
  int shipLen=0;
  int shipXwsLen=0;
  int nameLen=0;
  int xwsLen=0;
  for(auto p : Pilot::GetAllPilots()) {
    if(facLen     < p.GetFaction().GetName().length()) facLen = p.GetFaction().GetName().length();
    if(shipLen    < p.GetShip().GetName().length())    shipLen = p.GetShip().GetName().length();
    if(shipXwsLen < p.GetShip().GetXws().length())     shipXwsLen = p.GetShip().GetXws().length();
    if(nameLen    < p.GetName().length())              nameLen = p.GetName().length();
    if(xwsLen     < p.GetXws().length())               xwsLen  = p.GetXws().length();
  }
  for(auto p : pilots) {
    std::string facCol;
    switch(p.GetFaction().GetType()) {
    case Fac::Rebel:    facCol=CLR_REB; break;
    case Fac::Imperial: facCol=CLR_IMP; break;
    case Fac::Scum:     facCol=CLR_SCM; break;
    default:            facCol="";      break;
    }

    // print the xws pilot name
    printf(" %s%-*s",
           CLR_PLT, xwsLen, p.GetXws().c_str()
           );
    // print the faction (which changes the color)
    printf(" %s%-*s",
           facCol.c_str(),
           facLen, p.GetFaction().GetXws().c_str()
           );
    // print the ship (xws)
    printf(" %s%-*s",
           CLR_SHP, shipXwsLen, p.GetShip().GetXws().c_str()
           );
    // print the pilot name
    printf("   %s%s%s%-*s",
           CLR_UNI, p.IsUnique() ? "*" : " ", CLR_PLT, nameLen, p.GetName().c_str()
           );
    // print unreleased
    if(p.IsUnreleased()) {
      printf("%s[UNRELEASED]", CLR_UNR);
    }
    printf("\n");        
  }
}

void PrintPilot(Pilot p) {
  printf("%sPilot:   %s%s%s%s\n", CLR_LBL, CLR_UNI, p.IsUnique() ? "*" : "", CLR_PLT, p.GetName().c_str());
  printf("%sFaction: %s%s (%s)\n", CLR_LBL, CLR_TXT, p.GetFaction().GetName().c_str(), p.GetSubFaction().GetName().c_str());
  printf("%sShip:    %s%s\n", CLR_LBL, CLR_SHP, p.GetShip().GetName().c_str());
  printf("%sCost:    %s%d\n", CLR_LBL, CLR_CST, p.GetNatCost());
  printf("%sSkill:   %s%d\n", CLR_LBL, CLR_SKL, p.GetNatSkill());
  printf("%sStats:   %s%hhd %s%hhd %s%hhd %s%hhd\n", CLR_LBL, CLR_ATK, p.GetNatAttack(), CLR_AGI, p.GetNatAgility(),
	                                                      CLR_HUL, p.GetNatHull(),   CLR_SHD, p.GetNatShield());
  printf("%sArc:     %s%s\n", CLR_LBL, CLR_TXT, p.GetShip().GetPrimaryArc().GetName().c_str());
  printf("%sActions: %s",      CLR_LBL, CLR_TXT);
  bool x = 0;
  ForEachAction(p.GetNatActions(), [&x](Act a) {
      if(x) {
        printf(", ");
      }
      x = 1;
      printf("%s", Action::GetAction(a).GetName().c_str());
    });
  printf("\n");
  printf("%sUpgrades:%s", CLR_LBL, CLR_UPT);
  for(Upg u : p.GetNatPossibleUpgrades()) {
    printf(" %s", UpgradeType::GetUpgradeType(u).GetShortName().c_str());
  }
  printf("\n");
  printf("%s%s: %s%s\n", CLR_LBL, p.HasAbility() ? "Ability" : "Text", CLR_TXT, p.GetText().c_str());
  printf("%sAvailable: %s", CLR_LBL, CLR_TXT);
  std::vector<Release> rs = Release::GetPilot(p.GetXws(), p.GetFaction().GetType(), p.GetShip().GetType());
  bool isFirst = true;
  for(Release r : rs) {
    if(!isFirst) {
      printf(",");
    } else {
      isFirst = false;
    }
    printf(" %s", r.GetName().c_str());
  }
  printf("\n");
  printf("%sManeuvers:\n", CLR_LBL);
  PrintManeuverChart(p.GetNatManeuvers());
}



void PrintUpgrades(std::vector<Upgrade> upgrades) {
  int typeLen=0;
  int xwsLen=0;
  int nameLen=0;
  for(auto u : Upgrade::GetAllUpgrades()) {
    if(typeLen < u.GetType().GetXws().length()) typeLen = u.GetType().GetXws().length();
    if(xwsLen  < u.GetXws().length())           xwsLen  = u.GetXws().length();
    if(nameLen < u.GetName().length())          nameLen = u.GetName().length();
  }

  for(auto u : upgrades) {
    // print the upgrade type
    printf(" %s%-*s",
           CLR_UPT, typeLen, u.GetType().GetXws().c_str()
           );
    // print the upgrade xws name
    printf(" %s%-*s",
           CLR_UPG, xwsLen, u.GetXws().c_str()
           );
    // print the upgrade name
    printf(" %s%s%s%-*s",
           CLR_UNI, u.IsUnique() ? "*" : " ", CLR_UPG, nameLen, u.GetName().c_str()
           );
    // print unreleased
    if(u.IsUnreleased()) {
      printf("%s[UNRELEASED]", CLR_UNR);
    }
    printf("\n");
  }
}

void PrintUpgrade(Upgrade u) {
  int sides = u.IsDualSided() ? 2 : 1;
  for(int i=1; i<=sides; i++) {
    if(u.IsDualSided()) {
      printf("%sSide %d:\n", CLR_TXT, i);
    }
    printf("%s%sName:    %s%s%s%s\n", CLR_LBL, u.IsDualSided() ? " " : "", CLR_UNI, u.IsUnique() ? "*" : "", CLR_UPG, u.GetName().c_str());
    if(u.GetEnergyLimit()) {
      printf("%s%sEnergy:  %s%hhu\n", CLR_LBL, u.IsDualSided() ? " " : "", CLR_ENR, *u.GetEnergyLimit());
    }
    if(u.GetEnergyModifier()) {
      printf("%s%sEnergy:  %s%+hhd\n", CLR_LBL, u.IsDualSided() ? " " : "", CLR_ENR, *u.GetEnergyModifier());
    }
    if(u.GetAttackStats()) {
      std::string range;
      if(u.GetAttackStats()->minRange == u.GetAttackStats()->maxRange) {
	range = std::to_string(u.GetAttackStats()->minRange);
      } else {
	range = std::to_string(u.GetAttackStats()->minRange) + "-" + std::to_string(u.GetAttackStats()->maxRange);
      }
      printf("%s%sAttack:  %s%hhu%s (%s%s%s)\n",
	     CLR_LBL, u.IsDualSided() ? " " : "", CLR_ATK, u.GetAttackStats()->attack, CLR_TXT, CLR_RNG, range.c_str(), CLR_TXT);
    }
    printf("%s%sText:    %s%s\n", CLR_LBL, u.IsDualSided() ? " " : "", CLR_TXT, u.GetText().c_str());
    if(u.IsDualSided()) {
      u.Flip();
    }
  }

  printf("%sUnique:  %s%s\n",   CLR_LBL, CLR_TXT, u.IsUnique() ? "yes" : "no");
  printf("%sLimited: %s%s\n",   CLR_LBL, CLR_TXT, u.IsLimited() ? "yes" : "no");
  printf("%sType:    %s%s\n",   CLR_LBL, CLR_UPT, u.GetType().GetName().c_str());
  printf("%sCost:    %s%hhd\n", CLR_LBL, CLR_CST, u.GetCost());

  printf("%sAvailable:%s", CLR_LBL, CLR_TXT);
  std::vector<Release> rs = Release::GetUpgrade(u.GetType().GetType(), u.GetXws());
  bool isFirst = true;
  for(Release r : rs) {
    if(!isFirst) {
      printf(",");
    } else {
      isFirst = false;
    }
    printf(" %s", r.GetName().c_str());
  }
  printf("\n");
}



void PrintRelease() {
  printf("%sSKU   Name\n", CLR_TXT);
  int relLen=0;
  for(auto r : Release::GetAllReleases()) {
    if(relLen < r.GetName().length()) relLen = r.GetName().length();
  }

  for(auto r : Release::GetAllReleases()) {
    printf("%s%s %s%-*s%s%s\n", CLR_LBL, r.GetSku().c_str(), CLR_TXT, relLen, r.GetName().c_str(), CLR_UNR, r.IsUnreleased() ? " [UNRELEASED]" : "");
  }
}

void PrintRelease(std::string sku) {
  bool first = true;
  Release r = Release::GetRelease(sku);
  printf("%sName:  %s%s%s%s\n", CLR_LBL, CLR_TXT, r.GetName().c_str(), CLR_UNR, r.IsUnreleased() ? " [UNRELEASED]" : "");
  printf("%sSKU:   %s%s\n",     CLR_LBL, CLR_TXT, r.GetSku().c_str());
  printf("%sISBN:  %s%s\n",     CLR_LBL, CLR_TXT, r.GetIsbn().c_str());
  printf("%sAnnounced: %s%04d.%02d.%02d\n", CLR_LBL, CLR_TXT, r.GetAnnounceDate().year, r.GetAnnounceDate().month, r.GetAnnounceDate().date);
  printf("%sReleased:  %s%04d.%02d.%02d\n", CLR_LBL, CLR_TXT, r.GetReleaseDate().year, r.GetReleaseDate().month, r.GetReleaseDate().date);
  printf("\n");

  // ships
  first=true;
  for(RelShip s : r.GetShips()) {
    if(first) {
      printf("%sShip(s): ", CLR_LBL);
      first = false;
    } else {
      printf("         ");
    }
    printf("%s%s", CLR_SHP, Ship::GetShip(s.ship).GetName().c_str());
    if(s.desc != "") {
      printf(" (%s)", s.desc.c_str());
    }
    printf("\n");
  }

  printf("\n");

  // pilots
  first=true;
  for(RelPilot rp : r.GetPilots()) {
    if(first) {
      printf("%sPilot(s): ", CLR_LBL);
      first = false;
    } else {
      printf("          ");
    }
    Pilot p = Pilot::GetPilot(rp.name, Faction::GetFaction(rp.faction).GetXws(), Ship::GetShip(rp.ship).GetXws());
    printf("%s%s%s%s %s(%s%s %s %s%s%s)\n", CLR_UNI, p.IsUnique() ? "*" : " ", CLR_TXT, p.GetName().c_str(), CLR_TXT, CLR_PLT, p.GetXws().c_str(), p.GetFaction().GetXws().c_str(), CLR_SHP, p.GetShip().GetXws().c_str(), CLR_TXT);
  }
  if(!first) { printf("\n"); }

  // upgrades
  first=true;
  for(RelUpgrade ru : r.GetUpgrades()) {
    if(first) {
      printf("%sUpgrade(s): ", CLR_LBL);
      first = false;
    } else {
      printf("            ");
    }
    Upgrade u = Upgrade::GetUpgrade(UpgradeType::GetUpgradeType(ru.type).GetXws(), ru.name);
    printf("%s%s%s%s %s(%s%s %s%s%s)\n", CLR_UNI, u.IsUnique() ? "*" : " ", CLR_TXT, u.GetName().c_str(), CLR_TXT, CLR_UPT, UpgradeType::GetUpgradeType(ru.type).GetXws().c_str(), CLR_UPG, u.GetXws().c_str(), CLR_TXT);
  }
  if(!first) { printf("\n"); }

  // conditions
  first=true;
  for(Cnd c : r.GetConditions()) {
    if(first) {
      printf("%sCondition(s): ", CLR_LBL);
      first = false;
    } else {
      printf("              ");
    }
    printf("%s%s\n", CLR_TXT, Condition::GetCondition(c).GetName().c_str());
  }
  if(!first) { printf("\n"); }

  // tokens
  first=true;
  for(auto tt : r.GetTokens().GetTokenTypes()) {
    if(first) {
      printf("%sToken(s): ", CLR_LBL);
      first = false;
    } else {
      printf("          ");
    }

    printf("%s%sx%hu %s", CLR_TXT, r.GetTokens().GetTokenCount(tt)>9 ? "" : " ", r.GetTokens().GetTokenCount(tt), Token::GetToken(tt).GetName().c_str());
    if(tt == Tok::TargetLock) {
      printf(" %s(", CLR_LBL);
      bool first = true;
      auto tls = r.GetTokens().GetTargetLockTokens();
      for(auto tl : tls) {
        if(!first) { printf(", "); }
        printf("%s/%s", tl.first.c_str(), tl.second.c_str());
        first = false;
      }
      printf(")");
    }
    else if(tt == Tok::ID) {
      printf(" %s(", CLR_LBL);
      bool first = true;
      auto ids = r.GetTokens().GetIdTokens();
      for(auto id : ids) {
        if(!first) { printf(", "); }
        printf("%hhu", id);
        first = false;
      }
      printf(")");
    }
    printf("\n");
  }
  if(!first) { printf("\n"); }

  // urls
  if(r.GetAnnouncementUrl() != "") {
    printf("%sAnnouncement: %s%s\n", CLR_LBL, CLR_TXT, r.GetAnnouncementUrl().c_str());
  }
  if(r.GetPreviewUrls().size() > 0) {
    first=true;
    for(std::string p : r.GetPreviewUrls()) {
      if(first) {
        printf("%sPreview:      ", CLR_LBL);
        first = false;
      } else {
        printf("              ");
      }
      printf("%s%s\n", CLR_TXT, p.c_str());
    }
  }
  if(r.GetReleaseUrl() != "") {
    printf("%sRelease:      %s%s\n", CLR_LBL, CLR_TXT, r.GetReleaseUrl().c_str());
  }
}



void PrintSquad(Squad s) {
  printf("%s\"%s\" [%s]\n", CLR_TXT, s.GetName().c_str(), s.GetFaction().GetName().c_str());
  if(!s.GetDescription().empty()) {
    printf("%s\n", s.GetDescription().c_str());
  }
  printf("\n");

  for(auto p : s.GetPilots()) {
    printf("%s[%hhd] %s%s%s%s%s - %s%s\n",
           CLR_CST,p.GetModCost(), CLR_UNI, p.IsUnique() ? "*" : "", CLR_PLT, p.GetName().c_str(), CLR_TXT, CLR_SHP, p.GetShip().GetName().c_str());

    // skill
    printf("%s%hhu", CLR_SKL, p.GetNatSkill());
    if(p.GetNatSkill() != p.GetModSkill()) {
      printf("(%hhu)", p.GetModSkill());
    }
    printf("%s - ", CLR_TXT);

    // attack
    printf("%s%hhu", CLR_ATK, p.GetNatAttack());
    if(p.GetNatAttack() != p.GetModAttack()) {
      printf("(%hhu)", p.GetModAttack());
    }
    printf("%s/", CLR_TXT);

    // agility
    printf("%s%hhu", CLR_AGI, p.GetNatAgility());
    if(p.GetNatAgility() != p.GetModAgility()) {
      printf("(%hhu)", p.GetModAgility());
    }
    printf("%s/", CLR_TXT);

    // hull
    printf("%s%hhu", CLR_HUL, p.GetNatHull());
    if(p.GetNatHull() != p.GetModHull()) {
      printf("(%hhu)", p.GetModHull());
    }
    printf("%s/", CLR_TXT);

    // shield
    printf("%s%hhu", CLR_SHD, p.GetNatShield());
    if(p.GetNatShield() != p.GetModShield()) {
      printf("(%hhu)", p.GetModShield());
    }
    printf("%s - ", CLR_TXT);

    ForEachAction(p.GetModActions(), [](Act a) {printf("[%s]", Action::GetAction(a).GetName().c_str());});
    printf(" - ");
    for(Upg u : p.GetModPossibleUpgrades()) {
      printf("[%s%s%s]", CLR_UPT, UpgradeType::GetUpgradeType(u).GetShortName().c_str(), CLR_TXT);
    }
    printf("\nUpgrades:");
    for(auto u : p.GetAppliedUpgrades()) {
      printf(" [%s%s%s]", CLR_UPG, u.GetName().c_str(), CLR_TXT);
    }
    printf(NORMAL"\n");

    // draw the maneuver chart
    Maneuvers ms = p.GetModManeuvers();

    PrintManeuverChart(ms);
    printf("\n");
  }
}
