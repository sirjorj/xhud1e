#include "helpers.h"



std::vector<std::string> tokenize(std::string s, const char DELIMITER) {
  std::vector<std::string> ret;
  size_t start = s.find_first_not_of(DELIMITER), end=start;
  while (start != std::string::npos){
    end = s.find(DELIMITER, start);
    ret.push_back(trim(s.substr(start, end-start)));
    start = s.find_first_not_of(DELIMITER, end);
  }
  return ret;
}

std::string trim(std::string s) {
  // https://stackoverflow.com/q/25829143
  size_t first = s.find_first_not_of(' ');
  size_t last = s.find_last_not_of(' ');
  return s.substr(first, (last-first+1));
}
