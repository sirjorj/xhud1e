#include "builder.h"
#include "helpers.h"
#include "colors.h"
#include <iostream>
#include <experimental/optional>
#include <regex>



SquadBuilder::SquadBuilder(Squad s) : squad(s), isRunning(true) {
  this->squad.AddVendorSpecific("xhud");
  this->squad.GetVendorSpecific().SetUrl("https://gitlab.com/sirjorj/xhud1e");
  this->squad.GetVendorSpecific().SetBuilder("xhud1e overlay generator");
  
}




void SquadBuilder::Run() {
  do {
    std::string line;
    printf("%sbuilder> ", CLR_BPT);
    std::getline(std::cin, line);
    this->ParseCommand(line);
  } while(this->isRunning);
}



/*
qqq   - quit
h     - help

n     - name [string]
d     - description [string]

p     - pilot (show available)
_a    - add [string(s)]
_#    - [index] (INCOMPLETE)
__r   - remove
__m#  - moveto[newindex]

__u   - upgrade (show available)
___a  - add [string(s)]
___#  - index (INCOMPLETE)
____r - remove

w     - write out file [filename]
*/



const std::regex RX_QUIT(   R"(^qqq$)");                         // quit
const std::regex RX_HELP(   R"(^[?]$)");                         // help
const std::regex RX_NAME(   R"(^n [A-Za-z0-9,.'!@#$%^&*() ]+$)"); // name
const std::regex RX_DESC(   R"(^d [A-Za-z0-9,.'!@#$%^&*() ]+$)"); // description
const std::regex RX_PLTHELP(R"(^p$)");                           // pilot help
const std::regex RX_PLTADD( R"(^pa(( [a-z0-9.-]+(\/*(imperial|rebel|scum)\/[a-z0-9]+)?)+)$)"); // pilot add (just 1 for now, multiple later)
const std::regex RX_PLTREM( R"(^p[1-8]r$)");                     // pilot remove
const std::regex RX_PLTMOV( R"(^p[1-8]m[1-8]$)");                // pilot moveto
const std::regex RX_UPGHELP(R"(^p[1-8]u$)");                     // upgrade help
const std::regex RX_UPGADD( R"(p[1-8]ua(( [a-z0-9.-]+(\/*[a-z0-9]+)?)+))"); // upgrade add (1 for now, multiple later)
const std::regex RX_UPGREM( R"(^p[1-8]ur(( [a-z0-9.-]+)+)$)");   // upgrade remove (1 for now, multiple later)
const std::regex RX_WRITE(  R"(^w .+.xws$)"); 


void SquadBuilder::ParseCommand(std::string cmd) {

  if(cmd == "") {
    // print the current squad
    printf("%sName: %s%s%s\n", CLR_LBL, CLR_TXT, this->squad.GetName().c_str(), CLR_BLD);
    printf("%sDesc: %s%s%s\n", CLR_LBL, CLR_TXT, this->squad.GetDescription().c_str(), CLR_BLD);
    printf("%sCost: %s%d%s\n", CLR_LBL, CLR_TXT, this->squad.GetCost(), CLR_BLD);
    int pc=1;
    for(Pilot p : this->squad.GetPilots()) {
      printf("%s%d%s) %s%s%s%s %s(%s)%s [%d]\n", CLR_PLT, pc++, CLR_LBL, CLR_UNI, p.IsUnique() ? "*" : "", CLR_PLT, p.GetName().c_str(), CLR_SHP, p.GetShip().GetName().c_str(), CLR_BLD, p.GetModCost());
      std::vector<Upg> slots = p.GetModPossibleUpgrades();
      std::vector<Upgrade> applied, ordered;
      for(auto u : p.GetAppliedUpgrades()) { applied.push_back(u); }
      std::vector<Upgrade>::iterator it;
      for(Upg u : slots) {
	for(it=applied.begin(); it != applied.end(); ++it) {
	  if(it->GetType().GetType() == u) {
	    break;
	  }
	}
	if(it != applied.end()) {
	  printf("    %s[%5s] %s%s%s%s\n", CLR_UPT, UpgradeType::GetUpgradeType(u).GetShortName().c_str(), CLR_UNI, it->IsUnique() ? "*" : "", CLR_UPG, it->GetName().c_str());
	  ordered.push_back(*it);
	  applied.erase(it);
	} else {
	  printf("    %s[%5s]\n", BLACK, UpgradeType::GetUpgradeType(u).GetShortName().c_str());
	}
      }

      // see if there are extras...
      if(applied.size() > 0) {
	printf("%sExtra upgrades:%s\n", CLR_ERR, CLR_BLD);
	for(it=applied.begin(); it != applied.end(); ++it) {
	  printf("    %s%s\n", CLR_UPG, it->GetName().c_str());
	}
      }

      // more stuff here...
    }
    printf("\n%s", CLR_BLD);
  }

  else if(std::regex_match(cmd, RX_QUIT)) {
    this->isRunning = false;
  }

  else if(std::regex_match(cmd, RX_HELP)) {
    printf("%s", CLR_BLD);
    printf("Commands:\n");
    printf("  ?          - help\n");
    printf("  qqq        - quit\n");
    printf("             - print current squad (yes, that's a blank line)\n");
    printf("  n {name}   - set squad name\n");
    printf("  d {desc}   - set squad description\n");
    printf("  p          - show all available pilots\n");
    printf("  pa {plt}   - add pilot(s)\n");
    printf("  p#r        - remove pilot (by number)\n");
    printf("  p#u        - show all available upgrades for pilot\n");
    printf("  p#ua {upg} - add upgrade(s)\n");
    printf("  p#ur {upg} - remove upgrade\n");
    printf("  w {file}   - write squad to file (must end with '.xws')\n");
  }

  else if(std::regex_match(cmd, RX_NAME)) {
    this->squad.SetName(cmd.substr(2));
  }

  else if(std::regex_match(cmd, RX_DESC)) {
    this->squad.SetDescription(cmd.substr(2));
  }

  else if(std::regex_match(cmd, RX_PLTHELP)) {
    Shp ship;
    printf("%sUsage: 'p %sship%s %spilot%s'\n", CLR_BLD, CLR_SHP, CLR_BLD, CLR_PLT, CLR_BLD);
    for(auto p : Pilot::GetAllPilots(this->squad.GetFaction().GetType())) {
      if(p.GetBaseSize().GetType() != BSz::Huge) {
        if(ship != p.GetShip().GetType()) {
          ship = p.GetShip().GetType();
          printf("\n  %s%s%s", CLR_SHP, p.GetShip().GetXws().c_str(), CLR_PLT);
        }
        printf("  %s", p.GetXws().c_str());
      }
    }
    printf("%s\n", CLR_BLD);
  }

  else if(std::regex_match(cmd, RX_PLTADD)) {
    std::vector<std::string> t = tokenize(cmd);
    t.erase(t.begin());
    for(std::string pltStr : t) {
      std::vector<std::string> toks = tokenize(pltStr, '/');
      std::experimental::optional<Pilot> newPlt;
      if(toks.size() == 3) {
	try {
	  newPlt = Pilot::GetPilot(toks[0], toks[1], toks[2]);
	}
	catch(PilotNotFound pnf) {
	  printf("%s\n", pnf.what());
	}
      }
      else {
	std::vector<Pilot> plts = Pilot::FindPilot(pltStr, this->squad.GetFaction().GetType());
	if(plts.size() == 0) {
	  printf("  %sNo matching pilot found for '%s'%s\n", CLR_ERR, pltStr.c_str(), CLR_BLD);
	}
	else if(plts.size() > 10) {
	  printf("  %sSeveral matching pilots found for '%s'%s:\n", CLR_ERR, pltStr.c_str(), CLR_BLD);
	  Shp ship;
	  for(Pilot p : plts) {
	    if(ship != p.GetShip().GetType()) {
	      ship = p.GetShip().GetType();
	      printf("\n    %s%s%s", CLR_SHP, p.GetShip().GetXws().c_str(), CLR_PLT);
	    }
	    printf(" %s", p.GetXws().c_str());
	  }
	  printf("%s\n", CLR_BLD);
	}
	else if(plts.size() > 1) {
	  printf("  %sMultiple matching pilots found for '%s'%s:\n", CLR_ERR, pltStr.c_str(), CLR_BLD);
	  for(Pilot p : plts) {
	    printf("  %s%s%s/%s/%s%s%s\n",
		   CLR_PLT, p.GetXws().c_str(), CLR_BLD,
		   p.GetFaction().GetXws().c_str(),
		   CLR_SHP, p.GetShip().GetXws().c_str(), CLR_BLD);
	  }
	}
	else {
	  newPlt = std::move(plts[0]);
	}
      }
      if(newPlt) {
	this->squad.AddPilot(*newPlt);
	printf("  %sAdding %s%s%s in %s%s\n",
	       CLR_BLD, CLR_PLT, newPlt->GetName().c_str(), CLR_BLD,
	       CLR_SHP, newPlt->GetShip().GetName().c_str());
      }
    }
  }

  else if(std::regex_match(cmd, RX_PLTREM)) {
    uint8_t index = cmd[1] - 48;
    if(index > this->squad.GetPilots().size()) {
      printf("  %sUnable to delete pilot %hhu - there are only %zu pilots%s\n", CLR_ERR, index, this->squad.GetPilots().size(), CLR_BLD);
    }
    else {
      printf("  Removing %s%s%s in %s%s%s\n",
	     CLR_PLT, this->squad.GetPilots()[index-1].GetName().c_str(), CLR_BLD,
	     CLR_SHP, this->squad.GetPilots()[index-1].GetShip().GetName().c_str(), CLR_BLD);
      this->squad.GetPilots().erase(this->squad.GetPilots().begin() + index-1);
    }
  }

  else if(std::regex_match(cmd, RX_PLTMOV)) {
    printf("TODO: Pilot Move\n");
  }

  else if(std::regex_match(cmd, RX_UPGHELP)) {
    uint8_t pIndex = cmd[1]-48;
    if(pIndex > this->squad.GetPilots().size()) {
      printf("  %sUnable to upgrade %hhu - there are only %zu pilots%s\n", CLR_ERR, pIndex, this->squad.GetPilots().size(), CLR_BLD);
    }
    else {
      Pilot &p = this->squad.GetPilots()[pIndex-1];
      Upg upg;
      printf("%sUsage: '[pilotnum]u %stype%s %sname%s'", CLR_BLD, CLR_UPT, CLR_BLD, CLR_UPG, CLR_BLD);
      std::vector<Upg> options = p.GetModPossibleUpgrades();
      for(auto u : Upgrade::GetAllUpgrades()) {
	// does this pilot have a slot for this upgrade type
	if(std::find(options.begin(), options.end(), u.GetType().GetType()) != options.end()) {
	  // is it compatible
	  if(u.GetRestrictionCheck()(p,this->squad.GetPilots()).size() == 0) {
	    // print it
	    if(upg != u.GetType().GetType()) {
	      upg = u.GetType().GetType();
	      printf("\n%s  %s%s", BLUE, UpgradeType::GetUpgradeType(upg).GetXws().c_str(), CYAN);
	    }
	    printf(" %s", u.GetXws().c_str());
	  }
	}
      }
      printf("%s\n", CLR_BLD);
    }
  }

  else if(std::regex_match(cmd, RX_UPGADD)) {
    uint8_t pIndex = cmd[1]-48;
    if(pIndex > this->squad.GetPilots().size()) {
      printf("  %sUnable to add upgrade to %hhu - there are only %zu pilots%s\n", CLR_ERR, pIndex, this->squad.GetPilots().size(), CLR_BLD);
    }
    else {
      std::vector<std::string> t = tokenize(cmd);
      t.erase(t.begin());
      for(std::string upgStr : t) {
	std::vector<std::string> toks = tokenize(upgStr, '/');
	std::experimental::optional<Upgrade> newUpg;
	if(toks.size() == 2) {
	  try {
	    newUpg = Upgrade::GetUpgrade(toks[0], toks[1]);
	  }
	  catch(UpgradeNotFound unf) {
	    printf("%s\n", unf.what());
	  }
	}
	else {
	  std::vector<Upgrade> upgs = Upgrade::FindUpgrade(upgStr);
	  if(upgs.size() == 0) {
	    printf("  %sNo matching upgrade found for '%s'%s\n", CLR_ERR, upgStr.c_str(), CLR_BLD);
	  }
	  else if(upgs.size() > 10) {
	    printf("  %sSeveral matching upgrades found for '%s'%s:\n", CLR_ERR, upgStr.c_str(), CLR_BLD);
	    Upg up;
	    for(Upgrade u : upgs) {
	      if(up != u.GetType().GetType()) {
		up = u.GetType().GetType();
		printf("/n    %s%s%s", CLR_UPT, u.GetXws().c_str(), CLR_UPG);
	      }
	      printf(" %s", u.GetXws().c_str());
	    }
	    printf("%s\n", CLR_BLD);
	  }
	  else if(upgs.size() > 1) {
	    printf("  %sMultiple matching upgrades found for '%s'%s:\n", CLR_ERR, upgStr.c_str(), CLR_BLD);
	    for(Upgrade u : upgs) {
	      printf("  %s%s%s/%s%s%s\n",
		     CLR_UPT, u.GetType().GetXws().c_str(), CLR_BLD,
		     CLR_UPG, u.GetXws().c_str(), CLR_BLD);
	    }
	  }
	  else {
	    newUpg = std::move(upgs[0]);
	  }
	}
	if(newUpg) {
	  Pilot &p = this->squad.GetPilots()[pIndex-1];
	  p.ApplyUpgrade(*newUpg);
	  printf("  %sAdding %s%s%s to %s%s%s/%s%s\n",
		 CLR_BLD, CLR_UPG, newUpg->GetName().c_str(), CLR_BLD,
		 CLR_PLT, p.GetName().c_str(), CLR_BLD,
		 CLR_SHP, p.GetShip().GetName().c_str());
	}
      }
    }
  }

  else if(std::regex_match(cmd, RX_UPGREM)) {
    uint8_t pIndex = cmd[1]-48;
    if(pIndex > this->squad.GetPilots().size()) {
      printf("  %sUnable to remove upgrade from %hhu - there are only %zu pilots%s\n", CLR_ERR, pIndex, this->squad.GetPilots().size(), CLR_BLD);
    }
    else {
      std::vector<Upgrade> &aus = this->squad.GetPilots()[pIndex-1].GetAppliedUpgrades();
      if(aus.size() == 0) {
	printf("No upgrades to remove\n");
      }
      else {
	std::vector<std::string> t = tokenize(cmd);
	t.erase(t.begin());
	for(std::string upgStr : t) {
	  auto upgs = Upgrade::FindUpgrade(upgStr, aus);
	  if(upgs.size() == 0) {
	    printf("  %sNo matching upgrade found for '%s'%s\n", CLR_ERR, upgStr.c_str(), CLR_BLD);
	  }
	  else if(upgs.size() > 1) {
	    printf("  %sMultiple matching upgrades found for '%s'%s:", CLR_ERR, upgStr.c_str(), CLR_BLD);
	    Upg upg;
	    for(Upgrade u : upgs) {
	      if(upg != u.GetType().GetType()) {
		upg = u.GetType().GetType();
		printf("\n    %s%s%s", BLUE, u.GetType().GetXws().c_str(), CYAN);
	      }
	      printf(" %s", u.GetXws().c_str());
	    }
	    printf("%s\n", CLR_BLD);
	  }
	  else {
	    Upgrade &tgt = upgs[0];
	    std::vector<Upgrade> &aus = this->squad.GetPilots()[pIndex-1].GetAppliedUpgrades();
	    std::vector<Upgrade>::iterator it;
	    for(it=aus.begin(); it != aus.end(); it++) {
	      if((it->GetType().GetType() == tgt.GetType().GetType()) &&
		 (it->GetName() == tgt.GetName())) {
		break;
	      }
	    }
	    if(it != aus.end()) {
	      printf("  Removing %s%s\n", CLR_UPG, it->GetName().c_str());
	      aus.erase(it);
	    }
	  }
	}
      }
    }
  }
  

  else if(std::regex_match(cmd, RX_WRITE)) {
    std::vector<std::string> t = tokenize(cmd);
    if(this->squad.Save(t[1])) {
      printf("  Squad Saved\n");
    }
    else {
      printf("  %sError saving squad%s\n", CLR_ERR, CLR_BLD);
    }
  }

  else {
    printf("Unknown command\n");
    
  }

}
