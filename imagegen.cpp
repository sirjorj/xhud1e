#include "imagegen.h"
#include "libxwing/glyph.h"
#include <gd.h>
#include <string.h>



//   fonts
// title: Squarish Sans CT Regular SC
// stats: xwstats
const std::string iconsFont = "./fonts/xwing-miniatures.ttf";
const std::string shipsFont = "./fonts/xwing-miniatures-ships.ttf";
const std::string titleFont = "./fonts/SquarishSansCTRegularSC.ttf";
const std::string statsFont = "./fonts/xwstats.ttf";

static int SQUAD_WIDTH  =  381;
static int SQUAD_HEIGHT = 1080;

static int IMG_WIDTH  = 1920;
static int IMG_HEIGHT = 1080;


class Box {
public:
  static Box FromTLBR(int top, int left, int bottom, int right) {
    return Box(top, left, right-left+1, bottom-top+1);
  }
  static Box FromTLWH(int top, int left, int width, int height) {
    return Box(top, left, width, height);
  }
  int Top()    { return this->top; }
  int Left()   { return this->left; }
  int Width()  { return this->width; }
  int Height() { return this->height; }
  int Bottom() { return this->height + this->top - 1; }
  int Right()  { return this->width + this->left - 1; }
  Box MoveTL(int top, int left) { return Box(top, left, this->width, this->height); }
  
private:
  Box() { }
  Box(int t, int l, int w, int h) : top(t), left(l), width(w), height(h) { }
  int top, left, width, height;
};



struct ColorPalette {
  // background
  int clear;
  int bg;
  // destroyed
  int destroyed;
  // basics
  int white;
  int black;
  // dice
  int attackDice;
  int defenseDice;
  // functions
  int pilot;
  int skill;
  int attack;
  int arc;
  int arcD;
  int agility;
  int hull;
  int shield;
  int hitHull;
  int hitShield;
  int upgrade;
  int extra;
  // disabled functions
  int pilotD;
  int skillD;
  int attackD;
  int agilityD;
  int hullD;
  int shieldD;
  int hitHullD;
  int hitShieldD;
  int upgradeD;
  int extraD;
};

// http://www.had2know.com/technology/rgb-to-gray-scale-converter.html
ColorPalette GetColorPalette(gdImagePtr &img) {
  ColorPalette colors;
  colors.clear       = gdImageColorAllocateAlpha(img,   0, 0, 0, 255);
  colors.bg          = gdImageColorAllocateAlpha(img,   0, 0, 0, 32);
  colors.destroyed   = gdImageColorAllocateAlpha(img, 255, 0, 0, 192);
  colors.white       = gdImageColorAllocate(img, 255, 255, 255);
  colors.black       = gdImageColorAllocate(img,   0,   0,   0);
  colors.attackDice  = gdImageColorAllocate(img, 192,   0,   0);
  colors.defenseDice = gdImageColorAllocate(img,   0, 128,   0);
  colors.pilot       = gdImageColorAllocate(img, 255, 255, 255);
  colors.pilotD      = gdImageColorAllocate(img,  96,  96,  96);
  colors.skill       = gdImageColorAllocate(img, 245, 127,  32);
  colors.skillD      = gdImageColorAllocate(img, 151, 151, 151);
  colors.attack      = gdImageColorAllocate(img, 235,  26,  65);
  colors.attackD     = gdImageColorAllocate(img,  93,  93,  93);
  colors.arc         = gdImageColorAllocate(img, 142,  17,  42);
  colors.arcD        = gdImageColorAllocate(img,  76,  76,  76);
  colors.agility     = gdImageColorAllocate(img, 135, 209,  67);
  colors.agilityD    = gdImageColorAllocate(img, 171, 171, 171);
  colors.hull        = gdImageColorAllocate(img, 244, 239,  23);
  colors.hullD       = gdImageColorAllocate(img, 216, 216, 216);
  colors.shield      = gdImageColorAllocate(img,  99, 234, 246);
  colors.shieldD     = gdImageColorAllocate(img, 195, 195, 195);
  colors.hitHull     = gdImageColorAllocate(img,  61,  60,   6);
  colors.hitHullD    = gdImageColorAllocate(img,  54,  54,  54);
  colors.hitShield   = gdImageColorAllocate(img,  25,  59,  62);
  colors.hitShieldD  = gdImageColorAllocate(img,  49,  49,  49);
  colors.upgrade     = gdImageColorAllocate(img, 255, 255, 255);
  colors.upgradeD    = gdImageColorAllocate(img,  96,  96,  96);
  colors.extra       = gdImageColorAllocate(img, 132, 106,  55);
  colors.extraD      = gdImageColorAllocate(img,  48,  48,  48);
  return colors;
}





static Box GetTextSize(std::string text, std::string font, double size) {
  // [0,1] lower-left  X,Y
  // [2,3] lower-right X,Y
  // [4,5] upper-right X,Y
  // [6,7] upper-left  X,Y
  int brect[8];
  char *err = gdImageStringFT(0,         // img
			      &brect[0], // rect
			      0,         // color
			      (char*)font.c_str(), // font
			      size,      // size
			      0.0,       // angle
			      0,         // x
			      0,         // y
			      (char*)text.c_str()); // string
  if(err) { printf("%s\n", err); return Box::FromTLBR(0,0,0,0); }
  return Box::FromTLBR(brect[7], brect[6], brect[3], brect[2]);
}



static Box DrawText(std::string text, std::string font, double size, gdImagePtr img, int color, int x, int y) {
  int brect[8];
  char *err = gdImageStringFT(img,       // img
			      &brect[0], // rect
			      color,     // color
			      (char*)font.c_str(), // font
			      size,      // size
			      0.0,       // angle
			      x,         // x
			      y,         // y
			      (char*)text.c_str()); // string
  if(err) { printf("%s\n", err); return Box::FromTLBR(0,0,0,0); }
  return Box::FromTLBR(brect[7], brect[6], brect[3], brect[2]);
}

static std::string GetNatModString(uint8_t nat, uint8_t mod) {
  char s[8];
  if(nat == mod) {
    snprintf(s, sizeof(s), "%hhu", nat);
  } else {
    snprintf(s, sizeof(s), "%hhu(%hhu)", nat, mod);
  }
  return s;
}


static void DrawTitle(gdImagePtr img, std::string title, ColorPalette const &colors) {
  Box boxTitle = Box::FromTLWH(5, 560, 800, 50);
  gdImageFilledRectangle(img, boxTitle.Left(), boxTitle.Top(), boxTitle.Right(), boxTitle.Bottom(), colors.bg);

  std::string titleString = title;
  double titleSize = 24.0;

  int maxNameLength = boxTitle.Width();

  Box boxString = GetTextSize(titleString, titleFont, titleSize);
  while(boxString.Width() > maxNameLength) {
    titleString = titleString.erase(titleString.size()-1);
    boxString = GetTextSize(titleString, titleFont, titleSize);
  }

  int startX = ((maxNameLength - boxString.Width()) / 2);
  DrawText(titleString, titleFont, titleSize, img, colors.white, startX + boxTitle.Left(), boxTitle.Top()+33);
}

static int DrawSquadTitle(gdImagePtr img, Squad& squad, int xOffset, int yOffset, ColorPalette const &colors, bool hasInit=0) {
  // title -> the entire area this function covers
  // glyph -> the faction icon (leftmost part of title)
  // name  -> the squad name (the rest of the title)
  // define the box that will contain everything
  Box boxTitle = Box::FromTLWH(yOffset+5, 5, 370, 30);
  gdImageFilledRectangle(img, xOffset+boxTitle.Left(), boxTitle.Top(), xOffset+boxTitle.Right(), boxTitle.Bottom(), colors.bg);
  // prep the values
  std::string titleGlyph = IconGlyph::GetGlyph(squad.GetFaction().GetType());
  std::string titleName = squad.GetName();
  double glyphSize = 18.0;
  double nameSize = 16.0;
  // get the required sizes
  Box boxGlyph = GetTextSize(titleGlyph, iconsFont, glyphSize);
  Box boxName  = GetTextSize(titleName,  titleFont,  nameSize);
  int glyphNamePadding = 10;
  // downsize the text until it fits
  int maxNameLength = boxTitle.Width() - boxGlyph.Width() - glyphNamePadding;
  while(boxName.Width() > maxNameLength) {
    titleName = titleName.erase(titleName.size()-1);
    boxName = GetTextSize(titleName, titleFont, nameSize);
  }
  // an empty string will screw up the title text box size to this next line compensates
  if(boxName.Height() < 5) { boxName = Box::FromTLWH(boxName.Top(), boxName.Left(), boxName.Width(), 15); }
  // draw the glyph and name
  DrawText(titleGlyph, iconsFont, glyphSize, img, hasInit ? colors.attack : colors.white, xOffset+7, yOffset + 27);
  int startX = ((maxNameLength - boxName.Width()) / 2) + xOffset + boxGlyph.Width() + glyphNamePadding;
  DrawText(titleName,  titleFont, nameSize,  img,                           colors.white, startX, yOffset + 27);
  return boxTitle.Height();
}



static int DrawPilot(gdImagePtr img, Pilot& pilot, int xOffset, int yOffset, ColorPalette const &colors) {
  double skillFontSize = 20.0;
  double pilotFontSize = 20.0;
  double  costFontSize = 14.0;
  double statsFontSize = 20.0;
  double shipsFontSize = 26.0;
  std::string shipString    = ShipGlyph::GetGlyph(pilot.GetShip().GetType());
  std::string pilotString   = pilot.GetShortName();
  std::string costString    = std::to_string(pilot.GetModCost());
  std::string skillString   = GetNatModString(pilot.GetNatSkill(), pilot.GetModSkill());
  std::string arcString     = IconGlyph::GetGlyph(pilot.GetShip().GetPrimaryArc().GetType());
  std::string attackString  = GetNatModString(pilot.GetNatAttack(), pilot.GetModAttack());
  std::string agilityString = GetNatModString(pilot.GetNatAgility(), pilot.GetModAgility());
  std::string hullString    = GetNatModString(pilot.GetNatHull(), pilot.GetModHull());
  std::string shieldString  = GetNatModString(pilot.GetNatShield(), pilot.GetModShield());
  bool en = pilot.IsEnabled();

  // first we need to draw a box for this pilot.
  // height will be mostly constant but will vary a bit based on the number of upgrades.
  // we can still pre-calculate it all given that we have fixed sizes for everything...

  int upgHeight = ((pilot.GetAppliedUpgrades().size()+1)/2) * 21;
  int pilotHeight = 60;
  pilotHeight += upgHeight;
  pilotHeight += 20; // shield/hull
  pilotHeight += 5; // footer

  int yName = yOffset;
  int yStat = yOffset + 30;
  int yUpg  = yOffset + 60;
  int yHp   = yOffset + 60 + upgHeight + 5;

  // background transparent image to darken background
  Box bsPilot = Box::FromTLWH(yName, xOffset, 381, pilotHeight);
  gdImageFilledRectangle(img, bsPilot.Left(), bsPilot.Top(), bsPilot.Right(), bsPilot.Bottom(), colors.bg);

  // pilot
  Box bsShip = Box::FromTLWH(yName, 10, 30, 31);
  Box bsUniq = Box::FromTLWH(yName, 38, 10, 31);
  Box bsName = Box::FromTLWH(yName, 50, 310, 31); //GetTextSize(pilotString, titleFont, pilotFontSize);
  DrawText(shipString,  shipsFont, shipsFontSize, img, en?colors.pilot:colors.pilotD, xOffset+bsShip.Left(), bsShip.Top()+25);
  int pOff=0;
  if(pilot.IsUnique()) {
    DrawText(IconGlyph::GetGlyph(Ico::unique), iconsFont, pilotFontSize, img, en?colors.pilot:colors.pilotD, xOffset+bsUniq.Left(), bsUniq.Top()+25);
    pOff = 7;
  }
  DrawText(pilotString, titleFont, pilotFontSize, img, en?colors.pilot:colors.pilotD, xOffset+bsUniq.Left()+pOff, bsName.Top()+25);

  // cost
  Box bsCost = GetTextSize(costString, statsFont, costFontSize);
  DrawText(costString, statsFont, costFontSize, img, colors.skillD, xOffset+380-bsCost.Width(), yName+20);

  // skill
  Box bsSkill = Box::FromTLWH(yStat, 10, 60, 31);//GetTextSize(skillString, statsFont, skillFontSize);
  DrawText(skillString, statsFont, skillFontSize, img, en?colors.skill:colors.skillD, xOffset+bsSkill.Left(), bsSkill.Top()+25);

  // attack
  //   show arc
  /* this is almost good to go
  if(true) {
    int x, y;
    switch(pilot.GetShip().GetPrimaryArc().GetType()) {
    case Arc::Turret:    x = 60; y =  0; break;
    case Arc::FrontRear: x = 66; y = -1; break;
    case Arc::FrontHalf: x = 62; y =  0; break;
    default:             x = 61; y =  0; break;
    }
    Box bsArc = Box::FromTLWH(yStat+y, x, 50, 31); //GetTextSize(attackString, statsFont, statsFontSize);
    DrawText(arcString, iconsFont, statsFontSize+5, img, en?colors.arc:colors.arcD, xOffset+bsArc.Left(), bsArc.Top()+25);
  }
  */
  Box bsAttack = Box::FromTLWH(yStat, 70, 50, 31); //GetTextSize(attackString, statsFont, statsFontSize);
  DrawText(attackString, statsFont, statsFontSize, img, en?colors.attack:colors.attackD, xOffset+bsAttack.Left(), bsAttack.Top()+25);

  // agility
  Box bsAgility = Box::FromTLWH(yStat, 120, 50, 31); //GetTextSize(agilityString, statsFont, statsFontSize);
  DrawText(agilityString, statsFont, statsFontSize, img, en?colors.agility:colors.agilityD, xOffset+bsAgility.Left(), bsAgility.Top()+25);

  // hull
  Box bsHull = Box::FromTLWH(yStat, 170, 50, 31); //GetTextSize(hullString, statsFont, statsFontSize);
  DrawText(hullString, statsFont, statsFontSize, img, en?colors.hull:colors.hullD, xOffset+bsHull.Left(),  bsHull.Top()+25);

  // shield
  Box bsShield = Box::FromTLWH(yStat, 220, 50, 31); //GetTextSize(shieldString, statsFont, statsFontSize);
  DrawText(shieldString, statsFont, statsFontSize, img, en?colors.shield:colors.shieldD, xOffset+bsShield.Left(), bsShield.Top()+25);

  // actions
  std::string actionString;
  ForEachAction(pilot.GetModActions(), [&actionString](Act a){
      actionString += IconGlyph::GetGlyph(a);
      actionString += " ";
    });
  actionString.pop_back();
  DrawText(actionString, iconsFont, 12, img, colors.white, xOffset+275, yStat+19);

  // upgrades
  int uCount = 0;
  for(auto u : pilot.GetAppliedUpgrades()) {
    int uRow = uCount / 2;
    Box bsIcon = Box::FromTLWH(yUpg+(uRow*20), !(uCount%2) ? 5 : 190, 22, 21);
    Box bsUniq = Box::FromTLWH(yUpg+(uRow*20), bsIcon.Right()+0, 160, 21);
    Box bsText = Box::FromTLWH(yUpg+(uRow*20), bsIcon.Right()+7, 160, 21);

    // draw Extra Ordinance tokens
    if(u.GetExtras()) {
      Box eb = GetTextSize(IconGlyph::GetGlyphEx(u.GetType().GetType()), iconsFont, 15);
      for(int i=0; i<u.GetExtras(); i++) {
	DrawText(IconGlyph::GetGlyphEx(u.GetType().GetType()).c_str(), iconsFont, 15, img, en ? colors.extra : colors.extraD, xOffset+bsText.Left()+(eb.Width()*i), bsIcon.Top()+17);
      }
    }
    DrawText(IconGlyph::GetGlyph(u.GetType().GetType()).c_str(), iconsFont, 15, img, en ? colors.upgrade : colors.upgradeD, xOffset+bsIcon.Left(), bsIcon.Top()+17); // upgrade icon
    int uOff=0;
    if(u.IsUnique()) {
      DrawText(IconGlyph::GetGlyph(Ico::unique), iconsFont, 13, img, en ? colors.upgrade : colors.upgradeD, xOffset+bsUniq.Left(), bsUniq.Top()+17); // upgrade text
      uOff = 5;
    }
    DrawText(u.GetShortName().c_str(), titleFont, 13, img, en ? colors.upgrade : colors.upgradeD, xOffset+bsUniq.Left()+uOff, bsText.Top()+17); // upgrade text
    if(!u.IsEnabled()) {
      int y = ((bsIcon.Top() + bsIcon.Bottom()) / 2) + 3;
      gdImageLine(img, xOffset+bsIcon.Left(), y, xOffset+bsText.Right(), y, colors.white);
    }
    uCount++;
  }

  // current shield/hull
  int hpWidth = 360;
  int segments = pilot.GetModShield() + pilot.GetModHull();
  int segWidth =  hpWidth / segments;
  for(int i=0; i<pilot.GetModHull(); i++) {
    Box dummyBox = Box::FromTLWH(yHp, (segWidth*i)+10+2, segWidth-4, 10);
    gdImageFilledRectangle(img, xOffset+dummyBox.Left(), dummyBox.Top(), xOffset+dummyBox.Right(), dummyBox.Bottom(),
			   (i < (pilot.GetCurHull())) ? en?colors.hull:colors.hullD : en?colors.hitHull:colors.hitHullD);
  }
  for(int i=0; i<pilot.GetModShield(); i++) {
    Box dummyBox = Box::FromTLWH(yHp, (pilot.GetModHull()*segWidth)+(segWidth*i)+10+2, segWidth-4, 10);
    gdImageFilledRectangle(img, xOffset+dummyBox.Left(), dummyBox.Top(), xOffset+dummyBox.Right(), dummyBox.Bottom(),
			   (i < (pilot.GetCurShield())) ? en?colors.shield:colors.shieldD : en?colors.hitShield:colors.hitShieldD);
  }

  // cross out if disabled and hull is zero
  if(!en && !pilot.GetCurHull()) {
    gdImageSetThickness(img, 5);
    gdImageLine(img, bsPilot.Left(), bsPilot.Top(),    bsPilot.Right(), bsPilot.Bottom(), colors.destroyed);
    gdImageLine(img, bsPilot.Left(), bsPilot.Bottom(), bsPilot.Right(), bsPilot.Top(),    colors.destroyed);
  }

  
  return pilotHeight;
}



static void DrawSquad(gdImagePtr img, Squad &squad, uint8_t initiative, int xOffset, int yOffset, ColorPalette const &colors) {
  yOffset += DrawSquadTitle(img, squad, xOffset, yOffset, colors, initiative==1 ? 1 : 0);
  yOffset += 10;
  for(auto& pilot : squad.GetPilots()) {
    yOffset += DrawPilot(img, pilot, xOffset, yOffset, colors);
    yOffset += 10;  // some space between pilots
  }
}



uint8_t GetDefeatedPoints(Squad squad) {
  uint8_t ret = 0;
  bool hasLiving = false;
  for(Pilot &p : squad.GetPilots()) {
    if(p.GetCurHull() == 0) {
      ret += p.GetModCost();
    } else {
      hasLiving = true;
      if(p.GetBaseSize().GetType() == BSz::Large) {
	uint8_t totalHp = p.GetModHull() + p.GetModShield();
	uint8_t halfHp = totalHp / 2;
	if((p.GetCurHull() + p.GetCurShield()) <= halfHp) {
	  ret += p.GetModCost() / 2;
	}
      }
    }
  }
  if(!hasLiving) { ret = 100; }
  return ret;
}

bool OnlyNpRemains(Squad s) {
  if(s.GetFaction().GetType() != Fac::Scum) { return false; }
  for(Pilot &p : s.GetPilots()) {
    if(p.GetXws() == "nashtahpuppilot") {
      if(!p.IsEnabled()) { return false; }
    }
    else {
      if(p.IsEnabled()) { return false; }
    }
  }
  return true;
}

std::pair<uint8_t, uint8_t> GetScore(Squad s1, Squad s2) {
  uint8_t p1lost = GetDefeatedPoints(s1);
  uint8_t p2lost = GetDefeatedPoints(s2);
  // stupid nashtah pup...
  if     (OnlyNpRemains(s1) && p2lost==100) { return {101, 99};        }
  else if(OnlyNpRemains(s2) && p1lost==100) { return {99, 101};        }
  else                                      { return {p2lost, p1lost}; }
}

static void DrawScore(gdImagePtr img, bool p1, uint8_t value, ColorPalette const &colors) {
  std::string score = std::to_string(value);
  std::string font  = statsFont;
  double      size  = 36.0;
  Box bBack = GetTextSize("100", font, size+6.0); // size the background for worst-case ("100") and bump
  Box bText = GetTextSize(score, font, size);     // font size a bit because font rendering is stupid
  
  bBack = bBack.MoveTL(5, p1 ? SQUAD_WIDTH+5 : IMG_WIDTH-SQUAD_WIDTH-bBack.Width()-5);
  bText = bText.MoveTL(5, bBack.Left() + (bBack.Width()-bText.Width()));
  
  gdImageFilledRectangle(img, bBack.Left(), bBack.Top(), bBack.Right(), bBack.Height(), colors.bg);

  DrawText(score,
	   font,
	   size,
	   img,
	   colors.white,
	   bText.Left(),
	   bText.Top()+bText.Height());
}



static void DrawDice(gdImagePtr img, Dice& dice, int xOffset, int yOffset, ColorPalette const &colors) {
  int x = xOffset;
  int y = yOffset;

  if(dice.state == Dice::State::None) { return; }

  else if(dice.state == Dice::State::Attacking) {
    for(auto ad : dice.attackDice.GetSortedDice()) {
      Box boxTitle = Box::FromTLWH(y, x, 100, 100);
      gdImageFilledRectangle(img, boxTitle.Left(), boxTitle.Top(), boxTitle.Right(), boxTitle.Bottom(), colors.attackDice);
      Box bsIcon = DrawText(IconGlyph::GetGlyph(ad.GetType()).c_str(), iconsFont, 40, img, colors.white, 0, 0);
      DrawText(IconGlyph::GetGlyph(ad.GetType()).c_str(), iconsFont, 40, img, colors.white, x+(boxTitle.Width()-bsIcon.Width())/2, y+15+(boxTitle.Height())/2);
      y += boxTitle.Height() + 10;
    }
  }

  else if(dice.state == Dice::State::Defending) {
    for(auto dd : dice.defenseDice.GetSortedDice()) {
      Box boxTitle = Box::FromTLWH(y, x, 100, 100);
      gdImageFilledRectangle(img, boxTitle.Left(), boxTitle.Top(), boxTitle.Right(), boxTitle.Bottom(), colors.defenseDice);
      Box bsIcon = DrawText(IconGlyph::GetGlyph(dd.GetType()).c_str(), iconsFont, 40, img, colors.white, 0, 0);
      DrawText(IconGlyph::GetGlyph(dd.GetType()).c_str(), iconsFont, 40, img, colors.white, x+(boxTitle.Width()-bsIcon.Width())/2, y+15+(boxTitle.Height())/2);
      y += boxTitle.Height() + 10;
    }
  }

}



//
// this one generates an image of a single squad
//
void GenerateImage(Squad& squad, std::string name) {
  gdImagePtr img;

  // create the image
  img = gdImageCreateTrueColor(SQUAD_WIDTH, SQUAD_HEIGHT);

  // prep the color palette
  ColorPalette colors = GetColorPalette(img);

  // set transparent backgrounds
  gdImageSaveAlpha(img, 1);
  gdImageAlphaBlending(img, 0); // clear to enable transparent background
  for(int i=0; i<SQUAD_WIDTH; i++) {
    gdImageLine(img, i, 0, i, SQUAD_HEIGHT-1, colors.clear);
  }
  gdImageAlphaBlending(img, 1); // now that background is drawn, set this again to make fonts prettier

  // squad
  DrawSquad(img, squad, 0, 0, 0, colors);

  // save the image
  bool isStdout = (strcmp(name.c_str(), "--") == 0);
  FILE *out;
  if(isStdout) {
    out = stdout;
  } else {
    out = fopen(name.c_str(), "wb");
    if(out == 0) {
      printf("error opening file (%s)\n", name.c_str());
      return;
    }
  }
  gdImagePng(img, out);
  if(!isStdout) {
    fclose(out);
  }

  // clean up
  gdImageDestroy(img);  
}


//
// this one generates the overay image of both squads
//
void GenerateImage(ImageGenToken token) {
  gdImagePtr img;

  // generate one large image with both squads
  if(token.settings.GetSetting("run.multiimage") == "no") {

    // create the image
    img = gdImageCreateTrueColor(IMG_WIDTH, IMG_HEIGHT);

    // prep the color palette
    ColorPalette colors = GetColorPalette(img);

    // set transparent backgrounds
    gdImageSaveAlpha(img, 1);
    gdImageAlphaBlending(img, 0); // clear to enable transparent background
    for(int i=0; i<IMG_WIDTH; i++) {
      gdImageLine(img, i, 0, i, IMG_HEIGHT-1, colors.clear);
    }
    gdImageAlphaBlending(img, 1); // now that background is drawn, set this again to make fonts prettier

    // title
    if(token.settings.GetSetting("run.title") != "") {
      DrawTitle(img, token.settings.GetSetting("run.title"), colors);
    }

    std::pair<uint8_t, uint8_t> score = GetScore(token.p1squad, token.p2squad);
    
    // P1
    DrawSquad(img, token.p1squad, token.initiative==1?1:0, 0, 0, colors);
    DrawScore(img, true, score.first, colors);
    DrawDice(img, token.dice[0], SQUAD_WIDTH+5, 50, colors);

    // P2
    DrawSquad(img, token.p2squad, token.initiative==2?1:0, IMG_WIDTH-SQUAD_WIDTH, 0, colors);
    DrawScore(img, false, score.second, colors);
    DrawDice(img, token.dice[1], IMG_WIDTH-SQUAD_WIDTH-105, 50, colors);

    // save the image
    std::string file = token.settings.GetSetting("run.outdir");
    if(file[file.length()-1] != '/') { file += '/'; }
    file += "xhud.png";
    std::string tmpFile = file + ".tmp";
    FILE *out = fopen(tmpFile.c_str(), "wb");
    if(out == 0) {
      printf("error opening file (%s)\n", tmpFile.c_str());
      return;
    }
    gdImagePng(img, out);
    fclose(out);
    rename(tmpFile.c_str(), file.c_str());

    // clean up
    gdImageDestroy(img);  
  }

  // generate a separate image for each squad
  else {
    
    std::string rootDir = token.settings.GetSetting("run.outdir");
    if(rootDir[rootDir.length()-1] != '/') {
      rootDir += '/';
    }

    for(int i=1; i<=2; i++) {
      std::string filename = rootDir+"p"+ std::to_string(i) + ".png";
      FILE *out = fopen((filename+".tmp").c_str(), "wb");
      if(out == 0) {
	printf("error opening file (%s)\n", filename.c_str());
	return;
      }

      // create the image
      img = gdImageCreateTrueColor(SQUAD_WIDTH, SQUAD_HEIGHT);

      // prep the color palette
      ColorPalette colors = GetColorPalette(img);

      // set transparent backgrounds
      gdImageSaveAlpha(img, 1);
      gdImageAlphaBlending(img, 0); // clear to enable transparent background
      for(int i=0; i<SQUAD_WIDTH; i++) {
	gdImageLine(img, i, 0, i, SQUAD_HEIGHT-1, colors.clear);
      }
      gdImageAlphaBlending(img, 1); // now that background is drawn, set this again to make fonts prettier

      // squad
      DrawSquad(img, (i==1) ? token.p1squad : token.p2squad, token.initiative==i?1:0, 0, 0, colors);
      gdImagePng(img, out);
      fclose(out);
      rename((filename+".tmp").c_str(), filename.c_str());

      // clean up
      gdImageDestroy(img);  
    }
  }
}



std::string GetGdVersion() {
  return gdVersionString();
}
