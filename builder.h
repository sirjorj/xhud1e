#pragma once
#include "./libxwing/squad.h"

using namespace libxwing;

class SquadBuilder {
 public:
  SquadBuilder(Squad s);
  void Run();

 private:
  Squad squad;
  bool isRunning;
  void ParseCommand(std::string cmd);
};
