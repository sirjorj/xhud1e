#pragma once
#include "./libxwing/squad.h"
#include "imagegen.h"
#include <array>

using namespace libxwing;

class Game {
 public:
  Game(std::array<Squad, 2>& p, SettingsManager &s);
  void Run();

 private:
  ImageGenToken token;
  std::array<Squad, 2>& players;
  //std::string outFile;
  bool isRunning;
  bool ParseCommand(std::string cmd);
};
